var isNull = require('lodash').isNull;

exports.printMsg = function() {
    console.log(isNull(null));
    console.log(isNull(undefined));

    console.log("This is a message from the demo package");
};